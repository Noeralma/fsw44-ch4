const buyHouse = (balance) => {
    const housePrice = 300000000;

    if (balance >= housePrice) {
        return 'Beli Rumah!'
    }
    return 'Uang tidak cukup'
};

console.log(buyHouse(1000000000));

let shoppingDone = false;
let childsAllowance;

if (shoppingDone) {
  childsAllowance = 10;
} else {
  childsAllowance = 5;
}

console.log(childsAllowance);

const numberType = (number) => {
    if (number % 2 === 0 && typeof number === 'number') {
        return 'Genap';
    } else if (typeof number !== 'number') {
        return 'Input bukan angka';
    }
    return 'Ganjil';
};

console.log(numberType('abc'));
console.log(numberType(1));
console.log(numberType(-12));

const numberConverter = (number) => {
    if (typeof number === 'number') {
        if (number === 1) {
            return 'Satu';
        } else if (number === 2) {
            return 'Dua';
        } else if (number === 3) {
            return 'Tiga';
        } else if (number === 4) {
         return 'Empat';
        } else if (number === 5) {
            return 'Lima';
        } else if (number === 6) {
                return 'Enam';
        } else if (number === 7) {
            return 'Tujuh';
        } else if (number === 8) {
            return 'Delapan';
        } else if (number === 9) {
            return 'Sembilan';
        } else if (number === 0) {
            return 'Nol';
        }
        return 'Wait for update!'
    } 
    return 'Masukkan angka!'
};

console.log(numberConverter(10));