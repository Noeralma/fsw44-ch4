//comparison
const friedRice = 20000;
const friedNoodle = 30000;

const z = friedRice > friedNoodle;
const zTruth = friedRice < friedNoodle

console.log(z);
console.log(zTruth);

// equal
const kangkong = 'kangkung';
const spinach = 'bayam';

const zVeggy = kangkong == spinach;

console.log(zVeggy);

//strict equal
const one = 1;
const oneStrict = '1';

const truth = one == oneStrict;
const otherTruth = one === oneStrict;

console.log(otherTruth);
console.log(truth);

//strict unequal
const kangkong1 = 'kangkung';
const spinach1 = 'bayam';

const unVeggy = kangkong1 !== spinach1;

console.log(unVeggy);

//string comparison --> Melakukan comparison per index
const z5 = '10000' < '5000';
console.log(z5);